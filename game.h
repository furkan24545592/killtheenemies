#ifndef GAME_H
#define GAME_H
#endif

#include <iostream>
#include <ctime>
#include <string>
#include <cstdlib>

class Game
{
private:
    float MapX = 0, MapY = 0;
    int playerX = 0, playerY = 0, enemyX = 0, enemyY = 0, gemX = 0, gemY = 0, playerPower = 0, enemyPower = 0, gemCount_t = 0, gemCount = 0, greatX = 0, smallX = 0, greatY = 0, smallY = 0;
    char block = '*', player = 'P', enemy = 'E', gem = 'G';
    int tempCoordinateP = 0, tempCoordinateE = 0;
    std::string emptyArea = " ";
    bool isEnemyDead = false;

public:
    Game();
    Game(float, float);
    ~Game();
    void MapCreator();
    void newCoordinate(int);
    void attack();
    void canAttack(bool);
    void calculateAttackSituation();
    void update();
};