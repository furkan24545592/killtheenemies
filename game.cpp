#include "game.h"


//Constructor Function
Game::Game(float x, float y)
{
    MapX = x;
    MapY = y;

    srand (time(0));
    playerX = rand() % static_cast<int>(MapX - 2) + 2;  //Player X Random Coordinate
    playerY = rand() % static_cast<int>(MapY - 2) + 2;  //Player Y Random Coordinate

    enemyX =  rand() % static_cast<int>(MapX - 2) + 2;   //Enemy X Random Coordinate
    enemyY =  rand() % static_cast<int>(MapY - 2) + 2;   //Enemy Y Random Coordinate

    gemX  =   rand() % static_cast<int>(MapX - 2) + 2;   //Gem X Random Coordinate
    gemY  =   rand() % static_cast<int>(MapY - 2) + 2;   //Gem Y Random Coordinate
    
    
    if (playerX == enemyX && playerY == enemyY)         //If enemy and player random coordinate is same then redefine enemy coordinates
    {
        enemyX = rand() % static_cast<int>(MapX - 4) + 1;
        enemyY = rand() % static_cast<int>(MapY - 4) + 1;
    }
    
    if (enemyX == gemX && enemyY == gemY)               //If enemy and gem random coordinate is same then redefine gem coordinates
    {
        gemX = rand() % static_cast<int>(MapX - 5) + 1;
        gemY = rand() % static_cast<int>(MapY - 5) + 1;
    }
    
    system("cls");                                      //Clear screen when game is start
}

//Decstructor Function
Game::~Game()
{
    
}

//New area for gem
void Game::newCoordinate(int EnemyOrGem)
{

    switch (EnemyOrGem)
    {
    case 0:
        gemX  =   rand() % static_cast<int>(MapX - 2) + 2;   //Gem X new Random Coordinate
        gemY  =   rand() % static_cast<int>(MapY - 2) + 2;   //Gem Y new Random Coordinate

        if (playerX == gemX && playerY == gemY)
        {
            gemX = rand() % static_cast<int>(MapX - 4) + 1;
            gemY = rand() % static_cast<int>(MapY - 4) + 1;
        }
        
        if (enemyX == gemX && enemyY == gemY)
        {
            gemX = rand() % static_cast<int>(MapX - 5) + 1;
            gemY = rand() % static_cast<int>(MapY - 5) + 1;
        }
    break;

    case 1:
        enemyX  =   rand() % static_cast<int>(MapX - 2) + 2;   //Enemy X new Random Coordinate
        enemyY  =   rand() % static_cast<int>(MapY - 2) + 2;   //Enemy Y new Random Coordinate

        if (playerX == enemyX && playerY == enemyY) 
        {
            enemyX = rand() % static_cast<int>(MapX - 3) + 1;
            enemyY = rand() % static_cast<int>(MapY - 3) + 1;
        }
        
        if (gemX == enemyX && gemY == enemyY)               
        {
            enemyX = rand() % static_cast<int>(MapX - 4) + 1;
            enemyY = rand() % static_cast<int>(MapY - 4) + 1;
        }
    break;
    
    default:
        break;
    }


    system("cls");
    MapCreator();
}

//Map Creator Function
void Game::MapCreator()
{
    if (gemCount_t < gemCount)
    {
        gemCount_t = gemCount;
        newCoordinate(0);
    }
    else if(isEnemyDead == true)
    {
        isEnemyDead = false;
        newCoordinate(1);
    }
    
    
    for (float row = 1; row <= MapX; ++row)
    {
        std::cout << std::endl;
        for (float colomn = 1; colomn <= MapY; ++colomn)
        {
            if (row == 1 || row == MapX)
            {
                std::cout << block << emptyArea;
            }

            else if(row > 1 || row < MapX)
            {
                
                if (colomn == 1 || colomn == MapY)
                {
                    std::cout << block << emptyArea;
                }

            else
            {
                if (row == playerX && colomn == playerY)
                {
                    std::cout << player << emptyArea;
                    continue;
                }

                if (row == enemyX && colomn == enemyY)
                {
                    std::cout << enemy << emptyArea;
                    continue;
                }

                if (row == gemX && colomn == gemY)
                {   
                    std::cout << gem << emptyArea;
                    continue;
                }
                    
                    std::cout << emptyArea + emptyArea;
                }
            }
        }
    }

    tempCoordinateP = playerX + playerY;
    tempCoordinateE = enemyX + enemyY;

    std::cout << "\n\nPlayer Position X:" << playerX << "\nPlayer Position Y:" << playerY << "\n\n";
    std::cout << "Enemy Position X:" << enemyX << "\nEnemy Position Y:" << enemyY << "\n\n";
    std::cout << "Gem Position X:" << gemX << "\nGem Position Y:" << gemY << "\n\n";
    //std::cout << "TempP:" << tempCoordinateP << "\nTempE:" << tempCoordinateE << "\n";
    std::cout << "\nGem Count: " << gemCount << "\n";
    std::cout << "\nEnemy Power: " << enemyPower << "\n";

    update();
}

//Can player attack to enemy ?
void Game::canAttack(bool canAttack)
{
    if (canAttack == true)
    {
        attack();
    }
    else
    {
        std::cout << "No one here to attack" << std::endl;
        update();
    }   
}

//Player strength control
void Game::attack()
{
    if (gemCount <= enemyPower)
    {
        std::cout << "You are not strong enough" << std::endl;
        update();
    }
    else
    {
        std::cout << "Enemy Dead!" << std::endl;
        enemyPower += gemCount;
        isEnemyDead = true;
        MapCreator();
    }
    
    
    update();
}


void Game::calculateAttackSituation()
{
    greatX = (playerX > enemyX) ? playerX : enemyX;
    smallX = (enemyX < playerX) ? enemyX : playerX;
    greatY = (playerY > enemyY) ? playerY : enemyY;
    smallY = (enemyY < playerY) ? enemyY : playerY;

    if(smallX == greatX - 1 && smallY == greatY - 1)
    {
        if (tempCoordinateP == tempCoordinateE)
        {
            canAttack(true);
        }
        
        if (tempCoordinateP == tempCoordinateE - 1 || tempCoordinateE == tempCoordinateP - 1)       //For side by side situations
        {
            canAttack(true);
        }

        if (tempCoordinateP == tempCoordinateE - 2 || tempCoordinateE == tempCoordinateP - 2)       //
        {                                                           
            if (playerX != enemyX - 1 || playerX != enemyX + 1)                                     //  -> For cross situations
            {
                if (playerY != enemyY - 1 || playerY != enemyY + 1)                                 //
                {
                    canAttack(true);
                }
            }
        }

        else
        {
            canAttack(false);
        }
    }

    else if (enemyX == playerX || enemyY == playerY)
    {
        if (tempCoordinateP == tempCoordinateE - 1 || tempCoordinateE == tempCoordinateP - 1)       //For side by side situations
        {
            canAttack(true);
        }

        if (tempCoordinateP == tempCoordinateE - 2 || tempCoordinateE == tempCoordinateP - 2)       //
        {                                                           
            if (playerX == enemyX - 1 || playerX == enemyX + 1)                                     //  -> For cross situations
            {
                if (playerY == enemyY - 1 || playerY == enemyY + 1)                                 //
                {
                    canAttack(true);
                }
            }
            else
            {
                canAttack(false);
            }
        }
        else
        {
            canAttack(false);
        }
    }
    
    else if (enemyX == playerX - 1 && enemyY == playerY - 1)
    {
        if (tempCoordinateP == tempCoordinateE - 1 || tempCoordinateE == tempCoordinateP - 1)       //For side by side situations
        {
            canAttack(true);
        }
        if (tempCoordinateP == tempCoordinateE - 2 || tempCoordinateE == tempCoordinateP - 2)       //
        {                                                           
            if (playerX != enemyX - 1 || playerX != enemyX + 1)                                     //  -> For cross situations
            {
                if (playerY != enemyY - 1 || playerY != enemyY + 1)                                 //
                {
                    canAttack(true);
                }
            }
        }
        
        else
        {
            canAttack(false);
        }
    }
    else
    {
        canAttack(false);
    }
}


//Updating Game
void Game::update()
{
    char getKey;

    std::cout << "-> ";
    while (std::cin >> getKey)
    {
        if (getKey == '1')
        {
            calculateAttackSituation();
        }
        
        if (getKey == 'w')
        {
            playerX = playerX - 1;                          //Forward

            if (playerX == 1)
            {
                std::cout << "You fall out. Game Over.";
                exit(0);
            }
            
            else if (playerX == enemyX && playerY == enemyY)
            {
                std::cout << "You are dead.";
                exit(0);
            }

            else if (playerX == gemX && playerY == gemY)
            {
                gemCount = gemCount_t + 1;
                system("cls");
                MapCreator();
            }
            
            system("cls");
            MapCreator();
        }

        if (getKey == 'd')
        {
            playerY = playerY + 1;                          //Right

            if (playerY == MapY)
            {
                std::cout << "You fall out. Game Over.";
                exit(0);
            }
            else if (playerY == enemyY && playerX == enemyX)
            {
                std::cout << "You are dead.";
                exit(0);
            }
            else if (playerX == gemX && playerY == gemY)
            {
                gemCount = gemCount_t + 1;
                system("cls");
                MapCreator();
            }

            system("cls");
            MapCreator();
        }

        if (getKey == 'a')
        {
            playerY = playerY - 1;                          //Left

            if (playerY == 1)
            {
                std::cout << "You fall out. Game Over.";
                exit(0);
            }
            else if (playerY == enemyY && playerX == enemyX)
            {
                std::cout << "You are dead.";
                exit(0);
            }
            else if (playerX == gemX && playerY == gemY)
            {
                gemCount = gemCount_t + 1;
                system("cls");
                MapCreator();
            }

            system("cls");
            MapCreator();
        }

        if (getKey == 's')
        {
            playerX = playerX + 1;                          //Backward

            if (playerX == MapX)
            {
                std::cout << "You fall out. Game Over.";
                exit(0);
            }
            else if (playerX == enemyX && playerY == enemyY)
            {
                std::cout << "You are dead.";
                exit(0);
            }
            else if (playerX == gemX && playerY == gemY)
            {
                gemCount = gemCount_t + 1;
                system("cls");
                MapCreator();
            }

            system("cls");
            MapCreator();
        }

        if (getKey == 'q')                                  //Exit the game
        {
            exit(0);
        }
    }  
}